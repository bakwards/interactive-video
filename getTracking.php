<?php

$video = $_POST['videonr'];

require 'dbcon.php';

try 
{
    $dbh = new PDO($driver, $user, $pass, $attr);
}
catch (exception $e) 
{
    echo 'Exception : ' . $e->getMessage() . "<br />"; 
    die();
}

$xmlFil = "/home/mchriste/public_html/slumchallenge.dk/assets/xml/SlumDog.xml";
$xml = simplexml_load_file($xmlFil);

$ending=array();
$current_count = 0;

try
{        
    foreach($xml->children() as $child)
    {
        if($child->attributes()->FINAL == 1)
        {
            $temp = "SELECT COUNT(`video_id`) FROM `tracking` WHERE `video_id`=".$child->attributes()->ID." AND `article_name`=''";
            $result = $dbh->query($temp);
            
            if($result->rowCount()>0)
            {
                foreach($result as $item);
                {
                    if($child->attributes()->ID == $video){
                        $current_count = $item[0];
                    }
                    array_push($ending, array($child->attributes()->ID,$item[0]));
                }
            }
        }
    }
}
catch (exception $e)
{
    echo 'Exception : ' . $e->getMessage() . "<br />"; 
    die();
}

$views = 0;
$proc = array();

for ($row = 0; $row < count($ending); $row++)
{
    $views += $ending[$row][1];
}

//echo "<br />ID: " . $ending[$key][0] . " count: " . $ending[$key][1] . " procent har endt: " . number_format(($ending[$key][1] / $views)*100,0,'.','') . "% ialt har " . $views . " set filmen til ende.<br />";

echo number_format(($current_count / $views)*100,2,'.','');

?>