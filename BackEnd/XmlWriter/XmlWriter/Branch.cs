﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XmlWriter
{
    class Branch
    {
        public int GetId { get; set; }
        public string GetImg { get; set; }

        public Branch(int id, string img)
        {
            GetId = id;
            GetImg = img;
        }
    }
}
