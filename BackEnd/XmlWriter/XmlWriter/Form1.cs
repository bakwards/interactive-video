﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Data;
using System.Diagnostics;
using System.Diagnostics.Eventing;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using System.Xml;

namespace XmlWriter
{
    public partial class XMLWriter : Form
    {
        static private BindingList<Video> allVideos = new BindingList<Video>();
        static private Video temp;
        private XmlDocument document;

        private bool change = false;
        private Image videoImgFil;

        private string videoFilTemp;



        //Constructor
        public XMLWriter()
        {
            InitializeComponent();
        }

        #region Videolistegrid
        private void NewVideoBtn_Click(object sender, EventArgs e) //check if ID is free. NOT DONE!
        {
            int index = 0;
            for (int i = 0; i < allVideos.Count; i++)
            {
                if (allVideos[i].GetId == index)
                {
                    index++;
                }
                else
                {
                    foreach (Video a in allVideos)
                    {
                        if (a.GetId == index)
                        {
                            index++;
                        }
                    }
                }
            }
            temp = new Video(index);
            //find next free id
            VideoNr.Text = temp.GetId + "";
            allVideos.Add(temp);
            LoadAllVideoList();
            change = true;
        }
        private void VideoDataGrid()
        {
            foreach (DataGridViewCell cell in VideoListGrid.SelectedCells)
            {
                if (cell.Selected)
                {
                    VideoListGrid.Rows.RemoveAt(cell.RowIndex);
                }
            }
        }
        private void DeleteVideoBtn_Click(object sender, EventArgs e)
        {
            VideoDataGrid();
            //allVideos.Remove(temp);
            //temp = null;
            change = true;
        }
        private void LoadAllVideoList() //done
        {
            VideoListGrid.DataSource = null;
            VideoListGrid.DataSource = allVideos;
        }
        private void VideoListGrid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.RowIndex < VideoListGrid.RowCount)
            {
                temp = allVideos[e.RowIndex];
                //load fields
                VideoNr.Text = temp.GetId + "";
                VideoName.Text = temp.GetName;
                checkBox1.Checked = temp.GetFinal == 1 ? true : false;


                LoadBranchTree();
                LoadArticleDataGrid();
            }
        }
        private void LoadAllVideos(string filename)
        {
            //load from xml file
            allVideos.Clear();
            VideoNr.Text = "-";
            try
            {
                document = new XmlDocument();
                document.Load(filename);

                XmlNodeList nodes = document.DocumentElement.SelectNodes("SCENE");

                foreach (XmlNode node in nodes)
                {
                    temp = new Video(int.Parse(node.Attributes["ID"].Value),
                        node.Attributes["IMG"].Value,
                        int.Parse(node.Attributes["FINAL"].Value));

                    foreach (XmlNode n in node)
                    {
                        if (n.Name == "NAME")
                        {
                            temp.GetName = n.InnerText;
                        }
                        if (n.Name == "URL")
                        {
                            temp.GetUrl = n.InnerText;
                        }
                        if (n.Name == "JUNCTION")
                        {
                            foreach (XmlNode branc in n)
                            {
                                if (branc.Name == "BRANCH")
                                {
                                    temp.branch.Add(new Branch(int.Parse(branc.Attributes["ID"].Value),
                                                                branc.Attributes["IMG"].Value));
                                }
                            }
                        }
                        if (n.Name == "CONTENT")
                        {
                            foreach (XmlNode article in n)
                            {
                                if (article.Name == "ARTICLE")
                                {
                                    temp.articles.Add(new Article(int.Parse(article.Attributes["START"].Value),
                                                                int.Parse(article.Attributes["END"].Value),
                                                                article.Attributes["IMG"].Value,
                                                                article.Attributes["ALIGNMENT"].Value,
                                                                article.Attributes["URL"].Value));
                                }
                            }
                        }
                    }
                    allVideos.Add(temp);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: XML file not loaded. " + ex.ToString());
            }
            temp = null;
            LoadAllVideoList();
        } //done
        private void LoadXMLFileBtn_Click(object sender, EventArgs e)
        {
            //do you want to save before load?
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    if (openFileDialog1.FileName != "")
                    {
                        LoadAllVideos(openFileDialog1.FileName);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }
            }
        } //done
        private void SaveXMLFileBtn_Click(object sender, EventArgs e)
        {
            Stream myStream;
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();

            saveFileDialog1.Filter = "XML files (*.xml)|*.xml|All files (*.*)|*.*";
            saveFileDialog1.FilterIndex = 1;
            saveFileDialog1.RestoreDirectory = true;
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                if ((myStream = saveFileDialog1.OpenFile()) != null)
                {
                    // Code to write the stream goes here.
                    SaveXml(myStream);
                    myStream.Close();
                }
            }
        } //

        private void SaveXml(Stream stream)
        {
            XmlWriterSettings info = new XmlWriterSettings();
            info.Indent = true;

            using (System.Xml.XmlWriter writer = System.Xml.XmlWriter.Create(stream, info))
            {
                writer.WriteStartElement("VIDEOS");
                foreach (Video video in allVideos)
                {
                    writer.WriteStartElement("SCENE");
                    writer.WriteAttributeString("ID", video.GetId.ToString());
                    writer.WriteAttributeString("IMG", video.GetImg);
                    writer.WriteAttributeString("FINAL", video.GetFinal.ToString());
                    writer.WriteStartElement("NAME");
                    writer.WriteString(video.GetName);
                    writer.WriteEndElement();
                    writer.WriteStartElement("URL");
                    writer.WriteString(video.GetUrl);
                    writer.WriteEndElement();
                    writer.WriteStartElement("JUNCTION");
                    foreach (Branch branch in video.branch)
                    {
                        writer.WriteStartElement("BRANCH");
                        writer.WriteAttributeString("ID", branch.GetId.ToString());
                        writer.WriteAttributeString("IMG", branch.GetImg);
                        writer.WriteEndElement();
                    }
                    writer.WriteEndElement();
                    writer.WriteStartElement("CONTENT");
                    foreach (Article article in video.articles) //START="6" END="10" IMG="" ALIGNMENT="0" URL="">
                    {
                        writer.WriteStartElement("ARTICLE");
                        writer.WriteAttributeString("START", article.GetStartTime.ToString());
                        writer.WriteAttributeString("END", article.GetEndTime.ToString());
                        writer.WriteAttributeString("IMG", article.GetImg);
                        writer.WriteAttributeString("ALIGNMENT", article.GetPlacement.ToString());
                        writer.WriteAttributeString("URL", article.GetUml);
                        writer.WriteEndElement();
                    }
                    writer.WriteEndElement();
                    writer.WriteEndElement();
                }
                writer.WriteEndElement();
            }
        }



        #endregion
        #region video
        private void checkBox1_CheckedChanged(object sender, EventArgs e) //done
        {
            if (temp != null)
            {
                change = true;
                temp.GetFinal = checkBox1.Checked ? 1 : 0;
            }
        }
        private void LoadImgFil(string filname) //done (but more work on link
        {
            try
            {
                videoImgFil = Image.FromFile(filname);
                VideoImgPicBox.Image = videoImgFil.GetThumbnailImage(128, 128, null, IntPtr.Zero);
                temp.GetImg = filname;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: Could not read file. Original error: " + ex.Message);
            }
        }
        private void VideoImgButton_Click(object sender, EventArgs e) //done (but more work on link
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    if (openFileDialog1.FileName != "")
                    {
                        LoadImgFil(openFileDialog1.FileName);
                        change = true;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }
            }


        }
        private void VideoUrl_Click(object sender, EventArgs e) //done (but more work on link
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    if (openFileDialog1.FileName != "")
                    {
                        videoFilTemp = openFileDialog1.FileName;
                        temp.GetUrl = openFileDialog1.FileName;
                        change = true;
                        PlayVideo.Enabled = true;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }
            }
        }
        private void PlayVideo_Click(object sender, EventArgs e) //done
        {
            Process.Start(videoFilTemp);
        }
        private void VideoName_TextChanged(object sender, EventArgs e)
        {
            temp.GetName = VideoName.Text;
        }

        #endregion
        #region branchlistegrid
        private void LoadBranchTree() //done
        {
            BranchTreeData.DataSource = null;
            BranchTreeData.DataSource = temp.branch;
        }
        private void NewBrenchBtn_Click(object sender, EventArgs e)
        {
            temp.branch.Add(new Branch(temp.branch != null ? temp.branch.Count : 0, ""));
            LoadBranchTree();
        }
        private void DeleteBranch()
        {
            foreach (DataGridViewCell cell in BranchTreeData.SelectedCells)
            {
                if (cell.Selected)
                {
                    BranchTreeData.Rows.RemoveAt(cell.RowIndex);
                }
            }
        }
        private void DeleteBranchBtn_Click(object sender, EventArgs e)
        {
            DeleteBranch();
            change = true;
        }
        #endregion
        #region articlelistegrid
        private void LoadArticleDataGrid()
        {
            ArticlesDataGrid.DataSource = null;
            ArticlesDataGrid.DataSource = temp.articles;
        }
        private void NewArticleBtn_Click(object sender, EventArgs e)
        {
            temp.articles.Add(new Article(0, 0, "", "top left", ""));
            LoadArticleDataGrid();
        }
        private void DeleteArticles()
        {
            foreach (DataGridViewCell cell in ArticlesDataGrid.SelectedCells)
            {
                if (cell.Selected)
                {
                    ArticlesDataGrid.Rows.RemoveAt(cell.RowIndex);
                }
            }
        }
        private void DeleteArticleBtn_Click(object sender, EventArgs e)
        {
            DeleteArticles();
            LoadArticleDataGrid();
        }
        
        private void ArticlesDataGrid_CellLeave(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 3)
            {

                string test = ArticlesDataGrid.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString().ToLower();

                MessageBox.Show(test);
                //switch (switch_on)
                //{
                //    default:
                //}
            }
        }

        #endregion


        //Update And Tools
        private void GetSortetList() //Not done!
        {
            BindingList<Video> temp = new BindingList<Video>();
            int index = 0;
            int highest = 0;
            bool done = false;
            while (!done)
            {
                for (int i = 0; i < allVideos.Count; i++)
                {
                    if (allVideos[i].GetId == index)
                    {
                        temp.Add(allVideos[i]);

                    }
                    if (allVideos[i].GetId > index)
                    {

                    }
                    index++;
                }
            }
        }
        private void timer1_Tick(object sender, EventArgs e) //Update!
        {
            if (change)
            {
                SaveXMLFileBtn.Enabled = true;
                change = false;
                //autosave
                if (temp != null)
                {
                    allVideos[FindVideo(temp.GetId)] = temp;
                }

                VideoListGrid.Refresh();
                BranchTreeData.Refresh();
                ArticlesDataGrid.Refresh();

                //for (int i = 0; i < VideoListGrid.ColumnCount; i++)
                //{
                //    VideoListGrid.UpdateCellValue(i, VideoListGrid.SelectedRows.Count);
                //}

            }
        }
        private int FindVideo(int IdNr)
        {
            int theone = -1;

            for (int i = 0; i < allVideos.Count; i++)
            {
                if (allVideos[i].GetId == IdNr)
                {
                    if (theone == -1)
                    {
                        theone = i;
                    }
                    else
                    {
                        MessageBox.Show("Error: There are a Duplicate Id number, Check XML file!");
                    }
                }
            }

            return theone;
        } //done
        private void XMLWriter_Load(object sender, EventArgs e)
        {
            ToolTip tool = new ToolTip();

            tool.AutoPopDelay = 5000;
            tool.InitialDelay = 1000;
            tool.ReshowDelay = 500;
            tool.ShowAlways = true;

            tool.SetToolTip(this.VideoName, "Name of Video");
            //mm.
        }



        //dirt!
        private void Addalot_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < 54; i++)
            {
                NewVideoBtn_Click(this, null);
            }
        }















    }
}

