﻿namespace XmlWriter
{
    partial class XMLWriter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.IdLable = new System.Windows.Forms.Label();
            this.VideoImgButton = new System.Windows.Forms.Button();
            this.VideoImgPicBox = new System.Windows.Forms.PictureBox();
            this.VideoUrl = new System.Windows.Forms.Button();
            this.PlayVideo = new System.Windows.Forms.Button();
            this.NewBrenchBtn = new System.Windows.Forms.Button();
            this.VideoNr = new System.Windows.Forms.Label();
            this.NewVideoBtn = new System.Windows.Forms.Button();
            this.LoadXMLFileBtn = new System.Windows.Forms.Button();
            this.SaveXMLFileBtn = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.DeleteVideoBtn = new System.Windows.Forms.Button();
            this.DeleteBranchBtn = new System.Windows.Forms.Button();
            this.BrachImage = new System.Windows.Forms.PictureBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.VideoName = new System.Windows.Forms.TextBox();
            this.VideoListGrid = new System.Windows.Forms.DataGridView();
            this.NewXmlFile = new System.Windows.Forms.Button();
            this.ArticlesDataGrid = new System.Windows.Forms.DataGridView();
            this.NewArticleBtn = new System.Windows.Forms.Button();
            this.DeleteArticleBtn = new System.Windows.Forms.Button();
            this.updateCount = new System.Windows.Forms.Label();
            this.Addalot = new System.Windows.Forms.Button();
            this.BranchTreeData = new System.Windows.Forms.DataGridView();
            this.getStartTimeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.getEndTimeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.getImgDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.getPlacementDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.getUmlDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.articleBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.getIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.getNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.getImgDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.getFinalDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.getUrlDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.videoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.branchBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.getIdDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.getImgDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.VideoImgPicBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BrachImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VideoListGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ArticlesDataGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BranchTreeData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.articleBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.videoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.branchBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // IdLable
            // 
            this.IdLable.AutoSize = true;
            this.IdLable.Location = new System.Drawing.Point(18, 15);
            this.IdLable.Name = "IdLable";
            this.IdLable.Size = new System.Drawing.Size(48, 13);
            this.IdLable.TabIndex = 0;
            this.IdLable.Text = "Video ID";
            // 
            // VideoImgButton
            // 
            this.VideoImgButton.Location = new System.Drawing.Point(134, 12);
            this.VideoImgButton.Name = "VideoImgButton";
            this.VideoImgButton.Size = new System.Drawing.Size(75, 23);
            this.VideoImgButton.TabIndex = 1;
            this.VideoImgButton.Text = "Video Image";
            this.VideoImgButton.UseVisualStyleBackColor = true;
            this.VideoImgButton.Click += new System.EventHandler(this.VideoImgButton_Click);
            // 
            // VideoImgPicBox
            // 
            this.VideoImgPicBox.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.VideoImgPicBox.Location = new System.Drawing.Point(225, 12);
            this.VideoImgPicBox.Name = "VideoImgPicBox";
            this.VideoImgPicBox.Size = new System.Drawing.Size(128, 128);
            this.VideoImgPicBox.TabIndex = 2;
            this.VideoImgPicBox.TabStop = false;
            // 
            // VideoUrl
            // 
            this.VideoUrl.Location = new System.Drawing.Point(134, 41);
            this.VideoUrl.Name = "VideoUrl";
            this.VideoUrl.Size = new System.Drawing.Size(75, 23);
            this.VideoUrl.TabIndex = 3;
            this.VideoUrl.Text = "Video File";
            this.VideoUrl.UseVisualStyleBackColor = true;
            this.VideoUrl.Click += new System.EventHandler(this.VideoUrl_Click);
            // 
            // PlayVideo
            // 
            this.PlayVideo.Enabled = false;
            this.PlayVideo.Location = new System.Drawing.Point(134, 70);
            this.PlayVideo.Name = "PlayVideo";
            this.PlayVideo.Size = new System.Drawing.Size(75, 23);
            this.PlayVideo.TabIndex = 4;
            this.PlayVideo.Text = "Play Video";
            this.PlayVideo.UseVisualStyleBackColor = true;
            this.PlayVideo.Click += new System.EventHandler(this.PlayVideo_Click);
            // 
            // NewBrenchBtn
            // 
            this.NewBrenchBtn.Location = new System.Drawing.Point(409, 317);
            this.NewBrenchBtn.Name = "NewBrenchBtn";
            this.NewBrenchBtn.Size = new System.Drawing.Size(75, 23);
            this.NewBrenchBtn.TabIndex = 6;
            this.NewBrenchBtn.Text = "New Brench";
            this.NewBrenchBtn.UseVisualStyleBackColor = true;
            this.NewBrenchBtn.Click += new System.EventHandler(this.NewBrenchBtn_Click);
            // 
            // VideoNr
            // 
            this.VideoNr.AutoSize = true;
            this.VideoNr.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.VideoNr.Location = new System.Drawing.Point(15, 28);
            this.VideoNr.Name = "VideoNr";
            this.VideoNr.Size = new System.Drawing.Size(23, 31);
            this.VideoNr.TabIndex = 7;
            this.VideoNr.Text = "-";
            // 
            // NewVideoBtn
            // 
            this.NewVideoBtn.Location = new System.Drawing.Point(908, 317);
            this.NewVideoBtn.Name = "NewVideoBtn";
            this.NewVideoBtn.Size = new System.Drawing.Size(91, 23);
            this.NewVideoBtn.TabIndex = 8;
            this.NewVideoBtn.Text = "New";
            this.NewVideoBtn.UseVisualStyleBackColor = true;
            this.NewVideoBtn.Click += new System.EventHandler(this.NewVideoBtn_Click);
            // 
            // LoadXMLFileBtn
            // 
            this.LoadXMLFileBtn.Location = new System.Drawing.Point(909, 49);
            this.LoadXMLFileBtn.Name = "LoadXMLFileBtn";
            this.LoadXMLFileBtn.Size = new System.Drawing.Size(86, 30);
            this.LoadXMLFileBtn.TabIndex = 9;
            this.LoadXMLFileBtn.Text = "Load XML File";
            this.LoadXMLFileBtn.UseVisualStyleBackColor = true;
            this.LoadXMLFileBtn.Click += new System.EventHandler(this.LoadXMLFileBtn_Click);
            // 
            // SaveXMLFileBtn
            // 
            this.SaveXMLFileBtn.Location = new System.Drawing.Point(909, 85);
            this.SaveXMLFileBtn.Name = "SaveXMLFileBtn";
            this.SaveXMLFileBtn.Size = new System.Drawing.Size(86, 30);
            this.SaveXMLFileBtn.TabIndex = 10;
            this.SaveXMLFileBtn.Text = "Save XML File";
            this.SaveXMLFileBtn.UseVisualStyleBackColor = true;
            this.SaveXMLFileBtn.Click += new System.EventHandler(this.SaveXMLFileBtn_Click);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // DeleteVideoBtn
            // 
            this.DeleteVideoBtn.Location = new System.Drawing.Point(909, 346);
            this.DeleteVideoBtn.Name = "DeleteVideoBtn";
            this.DeleteVideoBtn.Size = new System.Drawing.Size(91, 23);
            this.DeleteVideoBtn.TabIndex = 11;
            this.DeleteVideoBtn.Text = "Delete";
            this.DeleteVideoBtn.UseVisualStyleBackColor = true;
            this.DeleteVideoBtn.Click += new System.EventHandler(this.DeleteVideoBtn_Click);
            // 
            // DeleteBranchBtn
            // 
            this.DeleteBranchBtn.Location = new System.Drawing.Point(409, 346);
            this.DeleteBranchBtn.Name = "DeleteBranchBtn";
            this.DeleteBranchBtn.Size = new System.Drawing.Size(75, 23);
            this.DeleteBranchBtn.TabIndex = 14;
            this.DeleteBranchBtn.Text = "Delete";
            this.DeleteBranchBtn.UseVisualStyleBackColor = true;
            this.DeleteBranchBtn.Click += new System.EventHandler(this.DeleteBranchBtn_Click);
            // 
            // BrachImage
            // 
            this.BrachImage.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.BrachImage.Location = new System.Drawing.Point(21, 207);
            this.BrachImage.Name = "BrachImage";
            this.BrachImage.Size = new System.Drawing.Size(128, 128);
            this.BrachImage.TabIndex = 17;
            this.BrachImage.TabStop = false;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(134, 121);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(85, 17);
            this.checkBox1.TabIndex = 19;
            this.checkBox1.Text = "Is it an End?";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // VideoName
            // 
            this.VideoName.Location = new System.Drawing.Point(21, 63);
            this.VideoName.Name = "VideoName";
            this.VideoName.Size = new System.Drawing.Size(100, 20);
            this.VideoName.TabIndex = 22;
            this.VideoName.TextChanged += new System.EventHandler(this.VideoName_TextChanged);
            // 
            // VideoListGrid
            // 
            this.VideoListGrid.AutoGenerateColumns = false;
            this.VideoListGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.VideoListGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.getIdDataGridViewTextBoxColumn,
            this.getNameDataGridViewTextBoxColumn,
            this.getImgDataGridViewTextBoxColumn,
            this.getFinalDataGridViewTextBoxColumn,
            this.getUrlDataGridViewTextBoxColumn});
            this.VideoListGrid.DataSource = this.videoBindingSource;
            this.VideoListGrid.Location = new System.Drawing.Point(501, 12);
            this.VideoListGrid.Name = "VideoListGrid";
            this.VideoListGrid.Size = new System.Drawing.Size(402, 357);
            this.VideoListGrid.TabIndex = 23;
            this.VideoListGrid.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.VideoListGrid_CellContentClick);
            this.VideoListGrid.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.VideoListGrid_CellContentClick);
            // 
            // NewXmlFile
            // 
            this.NewXmlFile.Location = new System.Drawing.Point(910, 12);
            this.NewXmlFile.Name = "NewXmlFile";
            this.NewXmlFile.Size = new System.Drawing.Size(86, 30);
            this.NewXmlFile.TabIndex = 24;
            this.NewXmlFile.Text = "New XML File";
            this.NewXmlFile.UseVisualStyleBackColor = true;
            // 
            // ArticlesDataGrid
            // 
            this.ArticlesDataGrid.AllowUserToAddRows = false;
            this.ArticlesDataGrid.AllowUserToDeleteRows = false;
            this.ArticlesDataGrid.AutoGenerateColumns = false;
            this.ArticlesDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ArticlesDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.getStartTimeDataGridViewTextBoxColumn,
            this.getEndTimeDataGridViewTextBoxColumn,
            this.getImgDataGridViewTextBoxColumn2,
            this.getPlacementDataGridViewTextBoxColumn,
            this.getUmlDataGridViewTextBoxColumn});
            this.ArticlesDataGrid.DataSource = this.articleBindingSource;
            this.ArticlesDataGrid.Location = new System.Drawing.Point(21, 458);
            this.ArticlesDataGrid.Name = "ArticlesDataGrid";
            this.ArticlesDataGrid.Size = new System.Drawing.Size(545, 137);
            this.ArticlesDataGrid.TabIndex = 26;
            this.ArticlesDataGrid.CellLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.ArticlesDataGrid_CellLeave);
            // 
            // NewArticleBtn
            // 
            this.NewArticleBtn.Location = new System.Drawing.Point(572, 542);
            this.NewArticleBtn.Name = "NewArticleBtn";
            this.NewArticleBtn.Size = new System.Drawing.Size(75, 23);
            this.NewArticleBtn.TabIndex = 27;
            this.NewArticleBtn.Text = "New Article";
            this.NewArticleBtn.UseVisualStyleBackColor = true;
            this.NewArticleBtn.Click += new System.EventHandler(this.NewArticleBtn_Click);
            // 
            // DeleteArticleBtn
            // 
            this.DeleteArticleBtn.Location = new System.Drawing.Point(572, 572);
            this.DeleteArticleBtn.Name = "DeleteArticleBtn";
            this.DeleteArticleBtn.Size = new System.Drawing.Size(75, 23);
            this.DeleteArticleBtn.TabIndex = 28;
            this.DeleteArticleBtn.Text = "Delete";
            this.DeleteArticleBtn.UseVisualStyleBackColor = true;
            this.DeleteArticleBtn.Click += new System.EventHandler(this.DeleteArticleBtn_Click);
            // 
            // updateCount
            // 
            this.updateCount.AutoSize = true;
            this.updateCount.Location = new System.Drawing.Point(982, 667);
            this.updateCount.Name = "updateCount";
            this.updateCount.Size = new System.Drawing.Size(13, 13);
            this.updateCount.TabIndex = 30;
            this.updateCount.Text = "0";
            // 
            // Addalot
            // 
            this.Addalot.Location = new System.Drawing.Point(828, 458);
            this.Addalot.Name = "Addalot";
            this.Addalot.Size = new System.Drawing.Size(75, 23);
            this.Addalot.TabIndex = 31;
            this.Addalot.Text = "Addalot";
            this.Addalot.UseVisualStyleBackColor = true;
            this.Addalot.Click += new System.EventHandler(this.Addalot_Click);
            // 
            // BranchTreeData
            // 
            this.BranchTreeData.AutoGenerateColumns = false;
            this.BranchTreeData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.BranchTreeData.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.getIdDataGridViewTextBoxColumn1,
            this.getImgDataGridViewTextBoxColumn1});
            this.BranchTreeData.DataSource = this.branchBindingSource;
            this.BranchTreeData.Location = new System.Drawing.Point(156, 207);
            this.BranchTreeData.MultiSelect = false;
            this.BranchTreeData.Name = "BranchTreeData";
            this.BranchTreeData.Size = new System.Drawing.Size(247, 162);
            this.BranchTreeData.TabIndex = 32;
            // 
            // getStartTimeDataGridViewTextBoxColumn
            // 
            this.getStartTimeDataGridViewTextBoxColumn.DataPropertyName = "GetStartTime";
            this.getStartTimeDataGridViewTextBoxColumn.HeaderText = "GetStartTime";
            this.getStartTimeDataGridViewTextBoxColumn.Name = "getStartTimeDataGridViewTextBoxColumn";
            // 
            // getEndTimeDataGridViewTextBoxColumn
            // 
            this.getEndTimeDataGridViewTextBoxColumn.DataPropertyName = "GetEndTime";
            this.getEndTimeDataGridViewTextBoxColumn.HeaderText = "GetEndTime";
            this.getEndTimeDataGridViewTextBoxColumn.Name = "getEndTimeDataGridViewTextBoxColumn";
            // 
            // getImgDataGridViewTextBoxColumn2
            // 
            this.getImgDataGridViewTextBoxColumn2.DataPropertyName = "GetImg";
            this.getImgDataGridViewTextBoxColumn2.HeaderText = "GetImg";
            this.getImgDataGridViewTextBoxColumn2.Name = "getImgDataGridViewTextBoxColumn2";
            // 
            // getPlacementDataGridViewTextBoxColumn
            // 
            this.getPlacementDataGridViewTextBoxColumn.DataPropertyName = "GetPlacement";
            this.getPlacementDataGridViewTextBoxColumn.HeaderText = "GetPlacement";
            this.getPlacementDataGridViewTextBoxColumn.Name = "getPlacementDataGridViewTextBoxColumn";
            // 
            // getUmlDataGridViewTextBoxColumn
            // 
            this.getUmlDataGridViewTextBoxColumn.DataPropertyName = "GetUml";
            this.getUmlDataGridViewTextBoxColumn.HeaderText = "GetUml";
            this.getUmlDataGridViewTextBoxColumn.Name = "getUmlDataGridViewTextBoxColumn";
            // 
            // articleBindingSource
            // 
            this.articleBindingSource.DataSource = typeof(XmlWriter.Article);
            // 
            // getIdDataGridViewTextBoxColumn
            // 
            this.getIdDataGridViewTextBoxColumn.DataPropertyName = "GetId";
            this.getIdDataGridViewTextBoxColumn.HeaderText = "GetId";
            this.getIdDataGridViewTextBoxColumn.Name = "getIdDataGridViewTextBoxColumn";
            this.getIdDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // getNameDataGridViewTextBoxColumn
            // 
            this.getNameDataGridViewTextBoxColumn.DataPropertyName = "GetName";
            this.getNameDataGridViewTextBoxColumn.HeaderText = "GetName";
            this.getNameDataGridViewTextBoxColumn.Name = "getNameDataGridViewTextBoxColumn";
            // 
            // getImgDataGridViewTextBoxColumn
            // 
            this.getImgDataGridViewTextBoxColumn.DataPropertyName = "GetImg";
            this.getImgDataGridViewTextBoxColumn.HeaderText = "GetImg";
            this.getImgDataGridViewTextBoxColumn.Name = "getImgDataGridViewTextBoxColumn";
            // 
            // getFinalDataGridViewTextBoxColumn
            // 
            this.getFinalDataGridViewTextBoxColumn.DataPropertyName = "GetFinal";
            this.getFinalDataGridViewTextBoxColumn.HeaderText = "GetFinal";
            this.getFinalDataGridViewTextBoxColumn.Name = "getFinalDataGridViewTextBoxColumn";
            // 
            // getUrlDataGridViewTextBoxColumn
            // 
            this.getUrlDataGridViewTextBoxColumn.DataPropertyName = "GetUrl";
            this.getUrlDataGridViewTextBoxColumn.HeaderText = "GetUrl";
            this.getUrlDataGridViewTextBoxColumn.Name = "getUrlDataGridViewTextBoxColumn";
            // 
            // videoBindingSource
            // 
            this.videoBindingSource.DataSource = typeof(XmlWriter.Video);
            // 
            // branchBindingSource
            // 
            this.branchBindingSource.DataSource = typeof(XmlWriter.Branch);
            // 
            // getIdDataGridViewTextBoxColumn1
            // 
            this.getIdDataGridViewTextBoxColumn1.DataPropertyName = "GetId";
            this.getIdDataGridViewTextBoxColumn1.HeaderText = "GetId";
            this.getIdDataGridViewTextBoxColumn1.Name = "getIdDataGridViewTextBoxColumn1";
            // 
            // getImgDataGridViewTextBoxColumn1
            // 
            this.getImgDataGridViewTextBoxColumn1.DataPropertyName = "GetImg";
            this.getImgDataGridViewTextBoxColumn1.HeaderText = "GetImg";
            this.getImgDataGridViewTextBoxColumn1.Name = "getImgDataGridViewTextBoxColumn1";
            // 
            // XMLWriter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 689);
            this.Controls.Add(this.BranchTreeData);
            this.Controls.Add(this.Addalot);
            this.Controls.Add(this.updateCount);
            this.Controls.Add(this.DeleteArticleBtn);
            this.Controls.Add(this.NewArticleBtn);
            this.Controls.Add(this.ArticlesDataGrid);
            this.Controls.Add(this.NewXmlFile);
            this.Controls.Add(this.VideoListGrid);
            this.Controls.Add(this.VideoName);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.BrachImage);
            this.Controls.Add(this.DeleteBranchBtn);
            this.Controls.Add(this.DeleteVideoBtn);
            this.Controls.Add(this.SaveXMLFileBtn);
            this.Controls.Add(this.LoadXMLFileBtn);
            this.Controls.Add(this.NewVideoBtn);
            this.Controls.Add(this.VideoNr);
            this.Controls.Add(this.NewBrenchBtn);
            this.Controls.Add(this.PlayVideo);
            this.Controls.Add(this.VideoUrl);
            this.Controls.Add(this.VideoImgPicBox);
            this.Controls.Add(this.VideoImgButton);
            this.Controls.Add(this.IdLable);
            this.Location = new System.Drawing.Point(100, 100);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "XMLWriter";
            this.Text = "XML Writer";
            this.Load += new System.EventHandler(this.XMLWriter_Load);
            ((System.ComponentModel.ISupportInitialize)(this.VideoImgPicBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BrachImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VideoListGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ArticlesDataGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BranchTreeData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.articleBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.videoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.branchBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label IdLable;
        private System.Windows.Forms.Button VideoImgButton;
        private System.Windows.Forms.PictureBox VideoImgPicBox;
        private System.Windows.Forms.Button VideoUrl;
        private System.Windows.Forms.Button PlayVideo;
        private System.Windows.Forms.Button NewBrenchBtn;
        private System.Windows.Forms.Label VideoNr;
        private System.Windows.Forms.Button NewVideoBtn;
        private System.Windows.Forms.Button LoadXMLFileBtn;
        private System.Windows.Forms.Button SaveXMLFileBtn;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button DeleteVideoBtn;
        private System.Windows.Forms.Button DeleteBranchBtn;
        private System.Windows.Forms.PictureBox BrachImage;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.TextBox VideoName;
        private System.Windows.Forms.DataGridView VideoListGrid;
        private System.Windows.Forms.Button NewXmlFile;
        private System.Windows.Forms.DataGridView ArticlesDataGrid;
        private System.Windows.Forms.Button NewArticleBtn;
        private System.Windows.Forms.Button DeleteArticleBtn;
        private System.Windows.Forms.Label updateCount;
        private System.Windows.Forms.DataGridViewTextBoxColumn getIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn getNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn getImgDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn getFinalDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn getUrlDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource videoBindingSource;
        private System.Windows.Forms.BindingSource branchBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn getStartTimeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn getEndTimeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn getImgDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn getPlacementDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn getUmlDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource articleBindingSource;
        private System.Windows.Forms.Button Addalot;
        private System.Windows.Forms.DataGridView BranchTreeData;
        private System.Windows.Forms.DataGridViewTextBoxColumn getIdDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn getImgDataGridViewTextBoxColumn1;
    }
}

