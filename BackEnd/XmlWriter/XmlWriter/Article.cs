﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace XmlWriter
{
    class Article
    {
        public int GetStartTime { get; set; }
        public int GetEndTime { get; set; }
        public string GetImg { get; set; }
        public string GetPlacement { get; set; }
        public string GetUml { get; set; }

        public Article(int start, int end, string img, string placement, string url)
        {
            GetStartTime = start;
            GetEndTime = end;
            GetImg = img;
            GetPlacement = placement.ToLower();
            GetUml = url;
        }
    }
}
