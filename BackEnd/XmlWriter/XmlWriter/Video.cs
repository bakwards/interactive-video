﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace XmlWriter
{
    class Video
    {
        public int GetId { get; private set; }
        public string GetName { get; set; }
        public string GetImg { get; set; }
        public int GetFinal { get; set; }

        public string GetUrl { get; set; }

        public BindingList<Branch> branch = new BindingList<Branch>();
        public BindingList<Article> articles = new BindingList<Article>();


        public Video(int id)
        {
            GetId = id;
            //dirty!
            GetUrl = "Sekvens " + id + ".mp4";
        }

        public Video(int id, string Img, int final)
        {
            GetId = id;
            GetImg = Img;
            GetFinal = final;
        }




    }
}
