/**
 * @author Christian Kajsing
 */

var currentVideo = null;
var sceneList = null;
var nextVideos = null;
var articles = null;

var videoID = null;
var videoIMG = null;
var videoUrl = null;
var videoName = null;
var xmlDoc;

//LoadFirstVideo();

//var txt;

//document.getElementById("showInfo").innerHTML = "Video ID: " + videoID + " Img link: " + videoIMG + " Video link: " + videoUrl;
//GetJunction();
//document.getElementById("NextVideos").innerHTML = txt;
//LoadNextVideo(3);
//document.getElementById("ShowChange").innerHTML = "Video ID: " + videoID + " Img link: " + videoIMG + " Video link: " + videoUrl;


function LoadXMLDoc(xmlName)
{
    if (window.XMLHttpRequest)
    {
        xmlDoc = new window.XMLHttpRequest();
        xmlDoc.open("GET", xmlName, true);
        xmlDoc.addEventListener("load", VideoInit, false);
        xmlDoc.send();
    }

}
function LoadFirstVideo()
{
    if (window.currentVideo == null)
    {
        var xmlFile = xmlDoc.responseXML;   // <-- S�T XML FIL HER!
        sceneList = xmlFile.getElementsByTagName('SCENE');
        currentVideo = sceneList[0];
    }
    else
    {
        alert("why did you call me? there are a current video!");
    }
    Update();
}
function LoadNextVideo(ID)
{
    for (var i = 0; i < sceneList.length; i++)
    {
        if (sceneList[i].getAttribute('ID') == ID)
        {
            currentVideo = sceneList[i];
            i = sceneList.length;
        }
    }
    Update();
}
function GetJunction()
{
    var tree = [];

    for (var i = 0; i < nextVideos.length; i++)
    {
        var obj = {
            ID: nextVideos[i].getAttribute('ID'),
            IMG: nextVideos[i].getAttribute('IMG')
        }
        tree[i] = obj;
    }
    return tree;
}
function GetArticles()
{
    var tmpArticles = [];

    for (var i = 0; i < articles.length; i++)
    {
        var obj = {
            START: articles[i].getAttribute('START'),
            END: articles[i].getAttribute('END'),
            IMG: articles[i].getAttribute('IMG'),
            ALIGNMENT: articles[i].getAttribute('ALIGNMENT'),
            URL: articles[i].getAttribute('URL')
        }
        tmpArticles[i] = obj;
    }
    return tmpArticles;
}

//En video: GetVideo(id){ return Object { URL(string), img(string), junction(array), articles(array) }}

function GetVideo(id)
{
    if (window.currentVideo == null) {
        LoadFirstVideo();
    }
    else {
        LoadNextVideo(id);
    }

    var obj = {
    	ID: videoID,
        URL: videoUrl,
        IMG: videoIMG,
        NAME: videoName,
        junction: GetJunction(),
        articles: GetArticles()
    }
    return obj;
}




function Update()
{
    articles = currentVideo.getElementsByTagName('ARTICLE');
    nextVideos = currentVideo.getElementsByTagName('BRANCH');
    videoID = currentVideo.getAttribute('ID');
    videoIMG = currentVideo.getAttribute('IMG');
    videoUrl = currentVideo.getElementsByTagName('URL')[0].childNodes[0].nodeValue;
    videoName = currentVideo.getElementsByTagName('NAME')[0].childNodes[0] !== undefined ? currentVideo.getElementsByTagName('NAME')[0].childNodes[0].nodeValue : "Go look for cats!";
}