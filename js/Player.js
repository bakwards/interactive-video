/**
 * @author Kristian Bak
 */
//initialization of global variables, html divs for articles, video, content list for endcard on first video

//global variables
var video = videojs('video');
var forkContentList = [];
var videoEl = video.el();
var articleButton = document.createElement("div");
var articleElement = document.createElement("div");
var articleContent = document.createElement("div");
var closeArticle = document.createElement("div");
var endCardContainer = document.createElement("ul");
var dashboard = document.getElementById("dashboard");
var dashboardArticles = document.getElementById("dashboardArticles");
var trackingBox = document.getElementById("trackingBox");
var restartButton = document.getElementById("restartButton");
var playButton = document.getElementById("playbutton");
var videoObj;
var percentCount = document.getElementById("percentcount");
var videoPath;
var didUseLink = false;
var usingTouch = !!('ontouchstart' in window) || !!('msmaxtouchpoints' in window.navigator);
LoadXMLDoc("assets/xml/SlumDog.xml");

$.post("tracking.php", { videonr: "-1" , article: ""}); /////////<<<<<<<-----TRACKING!

//class names and attributes
closeArticle.className = "closeArticle";
articleContent.className = "articleContent";
articleButton.setAttribute("onclick", "loadArticle()");
articleButton.className = "articleButton middle center";
closeArticle.setAttribute("onclick", "exitArticle()");
articleElement.className = "articleElement";
endCardContainer.className = "endCardContainer middle center";

//add elements to dom
videoEl.appendChild(articleElement);
videoEl.appendChild(articleButton);
videoEl.appendChild(endCardContainer);
videoEl.appendChild(dashboard);
videoEl.appendChild(dashboardArticles);
videoEl.appendChild(trackingBox);
//videoEl.appendChild(playbutton);
articleElement.appendChild(closeArticle);
articleElement.appendChild(articleContent);

//videoEl.addEventListener("touchstart", playPause());

var style = document.createElement("style");
style.type = "text/css";
document.head.appendChild(style);
//i know...

checkAspectRatio(true);
window.onresize = function(event){checkAspectRatio();};

function HashCheck(){
	if (document.location.hash != "")
    {
        switch (document.location.hash) {
            case "#jump":
                {
                    didUseLink = true;
                    video.currentTime(14);
                    playPause();
                    break;
                }
            default:
                {
                	var videoLink = document.location.hash.substring(1);
                    if (!isNaN(videoLink)) {
                    	didUseLink = true;
                  		playPause();
                        switchVideo(videoLink);
                    }
                    break;
                }
        }
    }
    if (document.location.search != null && document.location.search.substring(0, 6) == "?link=")
    {
        var str = document.location.search.substring(6);
        $.post("tracking.php", { link: str });
    }
}

//initialize video
function VideoInit(){
	videoObj = GetVideo(1);
	videoPath = new Path();
	video.dimensions("auto", "auto");
	video.cuepoints();
	updateVideoPlayer();
	video.on("loadedmetadata", function(){
		if(!didUseLink){
			HashCheck();
		}
		updateEndPoint();
		articleButton.className = "hidden";
		videoPath.pathDiv.className = "path hidden";
	});
}

//function for new video loaded

//playpause for clicking entire element and closing article
function playPause(){
	if(video.currentTime() == video.duration()){
		return;
	}
	if(DashboardOpen && video.paused()){
		OpenDashboard();
	} else {
		if(articleOpen){
			exitArticle();
			playButton.setAttribute("style", "opacity:0");
		} else if(video.paused()){
			video.play() ;
			playButton.setAttribute("style", "opacity:0");
		}else{
			video.pause();
			playButton.removeAttribute("style");
		}	
	}
}

function reloadVideo(){
	if(window.location == "http://www.slumchallenge.dk/#jump"){
		location.reload();
	}else {
		window.location.replace("http://www.slumchallenge.dk/#jump");
		location.reload();
	}
}

function checkForSpace(e){
	//console.log("key pressed: " + e.keyCode + ", cuepoints available: " + video.cuepoints.instances.length);
	switch(e.keyCode){
	case 32: //space
		playPause();
		break;
	case 66: //b
		video.currentTime(video.duration()-10);
		break;
	case 83: //s
		video.volume(0.05);
		break;
	/*
	case 39: //right
		nextArticle();
		break;
	case 37: //left
		prevArticle();
		break;
	*/
	default:
		break;
	}
}
function nextArticle(){
	for(var i = 0; video.cuepoints.instances.length > i; i++){
		if(video.cuepoints.instances[i].start > video.currentTime()+1){
			video.currentTime(video.cuepoints.instances[i].start);
			i = video.cuepoints.instances.length;
		}
	}
}
function prevArticle(){
	for(var i = video.cuepoints.instances.length-1; -1 < i; i--){
		if(video.cuepoints.instances[i].start < video.currentTime()-1){
			video.currentTime(video.cuepoints.instances[i].start);
			i = -1;
		}
	}
}


function checkAspectRatio(first){
	var aspectRatio = window.innerWidth / window.innerHeight;
	if(aspectRatio > 16/9){
		var newWidth = (16/9) / aspectRatio * 100;
		newWidth = Math.max(60, newWidth);
		videoEl.parentNode.setAttribute("style", "width: " + newWidth + "% ;");
		if(articleOpen && style.innerHTML.length < 2 || DashboardOpen && style.innerHTML.length < 2 || first){
			style.innerHTML = ".title{font-size: 5vh !important;} .floater{font-size: 2.3vm;font-size: 2.3vmin;} #dashboardArticles .titletext{font-size: 1.8vm;font-size: 2vmin;}";	
		}
	} else {
		videoEl.parentNode.removeAttribute("style");
		if(articleOpen && style.innerHTML.length > 2 || DashboardOpen && style.innerHTML.length > 2 ){
			style.innerHTML = "";		
		}
	}
	return aspectRatio;
}
