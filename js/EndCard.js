/**
 * @author Kristian Bak
 */
//generates fork content as li element.
function forkContent(id, image){
	var rel_content = document.createElement('li');
	if(image){
		rel_content.style.background = "url('http://cdn.slumchallenge.dk/assets/images/branch_icon/"+ image + "')";		
	} else {
		console.log ("you forgot or misplaced an image. it's ok, i inserted a cat.'");
		rel_content.style.background = "url('assets/images/cat.png')";
	}
	rel_content.setAttribute("onclick", "switchVideo('" + id + "')");
	rel_content.style.backgroundSize = "cover";
	//var NextVideo = GetVideo(id);
	//var name = NextVideo.NAME;
	//rel_content.innerHTML = "<p>" + name + "</p>";
	return rel_content;
}

//called by fork content and path nodes - updates video object, adds/updates node and calls updateVideoPlayer
function switchVideo(newVideo){
	var pathImage = "questionmark.png";
	for(var i = 0; i < nextVideos.length; i++){
		if(nextVideos[i].getAttribute('ID') == newVideo){
			pathImage = nextVideos[i].getAttribute('IMG');
		}
	}
	$.post("tracking.php", { videonr: videoID , article: ""}); /////////<<<<<<<-----TRACKING!
	videoObj = GetVideo(newVideo);
	videoPath.AddNode(pathImage);
	updateVideoPlayer();
	playPause();
}

//called to initialize and by switchVideo - set new src, updates fork contentlist, clears and updates cuepoints
function updateVideoPlayer(){
	var nextVideoUrl = videoObj.URL;
	if (navigator.userAgent.indexOf("Firefox")!=-1){
		nextVideoUrl = nextVideoUrl.substr(0, nextVideoUrl.lastIndexOf(".")) + ".webm";
	}
	video.src("http://cdn.slumchallenge.dk/assets/video/" + nextVideoUrl);
	video.load();
	updateContentList(videoObj.junction);
	video.clearCuepoints(); //this might generate a memoryleak - needs testing
	for(var i = 0; videoObj.articles.length > i; i++){
		var curArticle = videoObj.articles[i];
		addCuepoint(curArticle.START, curArticle.END, curArticle.URL, curArticle.ALIGNMENT, curArticle.IMG);
	}
}

//called by updateVideoPlayer - clear list, add new content and push to endcard array
function updateContentList(list){
	forkContentList = [];
	if(list instanceof Array){
		for(var i = 0; i < list.length; i++){
			forkContentList.push(forkContent(list[i].ID,list[i].IMG));
		}
	} else {
		throw new TypeError ("update content with an array next time. this time is fail");
	}
}