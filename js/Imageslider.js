function ImageSlider(sliderdiv, transition, rotation, dashboardSlide){
	this.container = document.createElement("div");
	this.leftBtn = document.createElement("div");
	this.rightBtn = document.createElement("div");
	this.container.className = "imageslider";
	this.leftBtn.className = "leftBtn";
	this.rightBtn.className = "rightBtn";
	this.array = [];
	this.currentSlide = 0;
	this.container.appendChild(this.leftBtn);
	this.container.appendChild(this.rightBtn);
	dashboardSlide ? this.leftBtn.setAttribute("onclick", "dashSlider.PrevSlide()") : this.leftBtn.setAttribute("onclick", "imageSlider.PrevSlide()");
	dashboardSlide ? this.rightBtn.setAttribute("onclick", "dashSlider.NextSlide()") : this.rightBtn.setAttribute("onclick", "imageSlider.NextSlide()");
	this.imageRotation;
	var textContainer = document.getElementById("textContainer");
	var sliderWidth = "100%";
	var slideTransition = new SlideTransition();
	var sliderDiv = sliderdiv;
	for(var i = 0; i < sliderDiv.childNodes.length; i++){
		if(sliderDiv.childNodes[i].nodeType === 1){
			var currentNode = sliderDiv.removeChild(sliderDiv.childNodes[i]);
			var image;
			var text;
			for(var o = 0; currentNode.childNodes.length > o; o++){
				if(currentNode.childNodes[o].nodeType === 1){
					switch(currentNode.childNodes[o].nodeName){
						case "IMG":
							image = currentNode.childNodes[o];
							break;
						case "DIV":
							text = currentNode.childNodes[o];
							break;
						default:
							break;
						}
				}
			}
			this.array.push(new SlideImage(image, text));
		}
	}
	sliderDiv.appendChild(this.container);
	this.Setup = function (transition, rotation){
		this.imageRotation = rotation;
		sliderWidth = this.container.offsetWidth;
		switch(transition){
			case "flick":
				slideTransition.forward = "left:"+sliderWidth+"px";
				slideTransition.back = "left:-"+sliderWidth+"px";
				slideTransition.center = "z-index: 100";
				break;
			case "zoom":
				slideTransition.forward = "-webkit-transform: scale(0.5, 0.5); transform: scale(0.5, 0.5); opacity: 0; z-index: 80";
				slideTransition.back = "-webkit-transform: scale(1.5, 1.5); transform: scale(1.5, 1.5); opacity: 0; z-index: 160";
				//slideTransition.forward = "-webkit-transform: scale(1, 1); transform: scale(1, 1); opacity: 0; z-index: 80";
				//slideTransition.back = "-webkit-transform: scale(6, 6); transform: scale(6, 6); opacity: 0; z-index: 160";
				slideTransition.center = "z-index: 100";
				break;
			default:
				slideTransition.forward = "left:"+sliderWidth+"px";
				slideTransition.back = "left:-"+sliderWidth+"px";
				slideTransition.center = "z-index: 100";
				break;
		}
		for(var i = 0; this.array.length > i; i++){
			this.container.appendChild(this.array[i].element);
			/*			 
			if(textContainer){
				textContainer.appendChild(this.array[i].textElement);
			}
			*/
			if(i>0){
				this.array[i].element.setAttribute("style", slideTransition.forward);
			}
			/*
			else {
				this.array[i].textElement.setAttribute("style", "opacity: 1; transition: none;");
			}
			*/
		}
		this.array[this.array.length -1].element.setAttribute("style", slideTransition.back);
		if(!this.imageRotation){
			//hide prev button
			this.leftBtn.setAttribute("style", "opacity: 0; left: -100px; transition: none;");
		}
	};
	this.NextSlide = function(){
		if(this.imageRotation || this.currentSlide < this.array.length - 1){
			if(this.currentSlide === 0 && !this.imageRotation){
				//show prev button
				this.leftBtn.removeAttribute("style");
			}
			this.array[this.currentSlide].element.setAttribute("style", slideTransition.back);
			//this.array[this.currentSlide].textElement.removeAttribute("style");
			if(this.currentSlide++ >= this.array.length -1){
				this.currentSlide = 0;
			}
			this.array[this.currentSlide].element.setAttribute("style", slideTransition.center);
			//this.array[this.currentSlide].textElement.setAttribute("style", "opacity: 1");
			if(this.currentSlide < this.array.length - 1){
				this.array[this.currentSlide+1].element.setAttribute("style", "transition:none; " + slideTransition.forward);
			} else {
				this.array[0].element.setAttribute("style", "transition:none; " + slideTransition.forward);
				if(!this.imageRotation){
					//hide next button
					this.rightBtn.setAttribute("style", "opacity: 0; right: -100px");
				}
			}
		}
	};
	this.PrevSlide = function(){
		if(this.imageRotation || this.currentSlide > 0){
			if(this.currentSlide === this.array.length -1 && !this.imageRotation){
				//show next button
				this.rightBtn.removeAttribute("style");
			}
			this.array[this.currentSlide].element.setAttribute("style", slideTransition.forward);
			//this.array[this.currentSlide].textElement.removeAttribute("style");
			if(this.currentSlide-- <= 0){
				this.currentSlide = this.array.length -1;
			}
			this.array[this.currentSlide].element.setAttribute("style", slideTransition.center);
			//this.array[this.currentSlide].textElement.setAttribute("style", "opacity: 1");
			if(this.currentSlide > 0){
				this.array[this.currentSlide-1].element.setAttribute("style", "transition:none; " + slideTransition.back);
			} else {
				this.array[this.array.length -1].element.setAttribute("style", "transition:none; " + slideTransition.back);
				if(!this.imageRotation){
					//hide prev button
					this.leftBtn.setAttribute("style", "opacity: 0; left: -100px");
				}
			}
		}
	};
	this.Setup(transition, rotation);
	return this;
}
function SlideImage(element, textelement){
	this.textElement = textelement;
	this.element = element;
}
function SlideTransition(){
	this.forward;
	this.back;
	this.center;
}