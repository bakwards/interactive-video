/**
 * @author Kristian Bak
 */

//path class - initializes and adds path to dom. generates first path node
function Path(){
	//initialize path and array
	this.pathDiv = document.createElement("ul");
	this.pathDiv.className = "path hidden";
	this.nodes = [];
	
	//add and return new node
	this.AddNode = function(pathImage){
		for(var i = 1; i < this.nodes.length; i++){
			this.nodes[i].nodeDiv.className = "node";
		}
		if(this.nodes.length > 0){
			this.nodes[this.nodes.length-1].UpdateImage(pathImage); //set node image to chosen branch image
		}		
		if(nextVideos.length > 0){
			var newNode = new Node(this.nodes.length);
			this.nodes.push(newNode);
			this.pathDiv.appendChild(newNode.nodeDiv);
			return newNode;
		}
	};
	
	//load video from node, remove all nodes after this one
	this.GoToNode = function(number){
		//id node and update player with video from node
		//i think we need a transition to new endcard
		if(number < this.nodes.length-1 ||
			trackingBox.className == "show fillall"){
			trackingBox.className = "hidden fillall";
			videoObj = GetVideo(this.nodes[number].id);
			var timeToGoTo = this.nodes[number].time;
			updateVideoPlayer();
			video.play();
			video.one("loadedmetadata",function (){video.currentTime(timeToGoTo)}); //works with video on server - not locally
		}
		//remove from array, dom and memory
		for(var i = this.nodes.length-1; number < i; i--){
			var element = this.nodes.pop();
			this.pathDiv.removeChild(element.nodeDiv);
			delete element;
		}
		//if not the startnode, this is the current node - first node is special (only styling)!
		if(number > 0){
			this.nodes[number].nodeDiv.className = "node currentNode";
		}
	};
	//get the last node and set time to time: used to find the endpoint minus 10 seconds
	this.UpdateNodePoint = function(time){
		this.nodes[this.nodes.length-1].time = time;
	};
	//initialize first node as start node
	this.AddNode().nodeDiv.className = "node start";
	videoEl.appendChild(this.pathDiv);
}

//node object with data from xml - initialized as li
function Node(number){
	this.nodeDiv = document.createElement("li");
	this.img = videoObj.IMG.length > 0 ? videoObj.IMG : "questionmark.png";
	this.id = videoObj.ID;
	this.time = video.duration() - 10;
	this.nodeDiv.setAttribute("onclick","videoPath.GoToNode("+number+")");
	this.nodeDiv.setAttribute("style", "background-image: url('assets/images/" + this.img + "')");
	this.nodeDiv.className = "node currentNode";
	this.UpdateImage = function(image){
		this.img = image;
		this.nodeDiv.setAttribute("style", "background-image: url('assets/images/branch_icon/" + image + "')");
	};
}