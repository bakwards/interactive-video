/**
 * @author Kristian Bak
 */
var articleOpen = false;
var imageSlider;
var currentArticle = "";
var articleVideo = null;

//show the article element, adjust button and player
function loadArticle(){ 
	video.pause();
	$.post("tracking.php", { videonr: "" , article: "Video: " + videoID + ", article: " + currentArticle}); /////////<<<<<<<-----TRACKING!
	articleOpen = true;
	oldButtonClass = articleButton.className;
	articleElement.className += " show";
	articleButton.className += " hidden";
	if(articleElement.getElementsByTagName("video").length > 0){
		articleVideo = articleElement.getElementsByTagName("video")[0];
	}
}
//hide article element
function exitArticle(){
	articleOpen = false;
	articleElement.className = "articleElement";
	if(articleVideo != null){
		articleVideo.pause();
		articleVideo = null;
	}
	playPause();
	
}
//add cue point - set a starttime and endtime to show the button, and updates article content onstart from html
function addCuepoint(startSecond, endSecond, content, alignment, image){
	image = image.length !== 0 ? image : "cat.png";
	video.addCuepoint({
	    namespace: "cuecat",
	    start: startSecond,
	    end: endSecond,
	    onStart: function(params){
	        if(params.error){
	            console.error("Error at second " + startSecond);
	        }else{
				var htmlDoc;
				var contentHtml = "Error contacting server";
				if(window.XMLHttpRequest){
					htmlDoc = new window.XMLHttpRequest();
					currentArticle = content;
					content = content.length > 0 ? content : "biggerarticle.html";
					htmlDoc.open("GET","assets/articles/" + content, true);
					htmlDoc.send();
					htmlDoc.addEventListener("load", function(e){
						articleButton.className = "articleButton fadeUp tilt " + alignment;
						articleButton.setAttribute("style", "background-image:url('http://cdn.slumchallenge.dk/assets/images/articleicons/"+image+"');");
						contentHtml = htmlDoc.responseText;
			            articleContent.innerHTML = contentHtml;
						var sliderDiv = document.getElementById("slider");
						var quizDiv = document.getElementById("Quiz");
						if(sliderDiv){
							imageSlider = new ImageSlider(sliderDiv, sliderDiv.getAttribute("transition"), sliderDiv.getAttribute("imagerotation") == 'true');
						}
						if(quizDiv){
							LoadQuestion(quizDiv.getAttribute("question"), quizDiv.getAttribute("answers"), quizDiv.getAttribute("correctanswer"));
						}
					}, false);
				}
	        }
	    },
	    onEnd: function(params){
	    	//hackcheck to see if the browser is safari, if so realign to center (which is outside the main div)
	    	if(navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Version') != -1 && parseFloat(navigator.userAgent.substring(navigator.userAgent.indexOf('Version') + 8).split(' ')[0]) >= 5){
	    		alignment = "middle center";
	    	}
	        articleButton.className = "articleButton fadeDown " + alignment;
	    },
	    params: {error: false}
	});
}
//endpoint set at fixed -10 seconds, show fork and nodes on endpointtime, updates nodepoint when showing nodes
var trackingPath = null;
var didSendData = false;
function updateEndPoint(){
	endCardContainer.className = "endCardContainer center middle";
	while(endCardContainer.childNodes.length > 0){
		endCardContainer.removeChild(endCardContainer.childNodes[0]);
	}
	var endPointTime = video.duration()-10;
	if(forkContentList.length === 0 && !didSendData){
		$.post("tracking.php", { videonr: videoID , article: ""}); /////////<<<<<<<-----TRACKING!
		didSendData = true;
		endPointTime = video.duration()-1;
	}
	video.addCuepoint({
	    namespace: "endcue",
	    start: endPointTime,
	    onStart: function(params){
	        if(params.error){
	            console.error("Error at second ");
	        }else{
				for(thing in forkContentList){
					endCardContainer.appendChild(forkContentList[thing]);
				}
	            //we ought to check the articlebutton to force it to fade away, but right now it's all up to the content xml - articles should never show late in the video
				endCardContainer.className = "endCardContainer fadeUp center middle";
				videoPath.UpdateNodePoint(endPointTime);
				if(videoPath.nodes.length > 1 && endCardContainer.childNodes.length !== 0){
	        		videoPath.pathDiv.className = "path show";
				}
				if(endCardContainer.childNodes.length === 0){
					if(trackingPath != null){
						trackingBox.removeChild(trackingPath);
					}
					$.post("getTracking.php",{ videonr: videoID},function(data,status){ //tracking call
				    	console.log("Data: " + data + "\nStatus: " + status);
				    	if(!isNaN(data)){
				    		console.log("Is actual data");	    		
			    			percentCount.innerText = data;
				    	} else {
				  		  percentCount.innerText = Math.round(Math.random(0,1) * 10000) / 100;
				    	}
				    });
					trackingPath = videoPath.pathDiv.cloneNode(true);
					trackingPath.className = "path show";	
					trackingBox.className = "show fillall";
					trackingBox.appendChild(trackingPath);
					trackingBox.appendChild(restartButton);
					didSendData = false;
				}
	        }
	    },
	    params: {error: false}
	});
}
