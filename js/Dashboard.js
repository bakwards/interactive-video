var DashboardOpen = false;
var OpenDashboardArticle = undefined;

function OpenDashboard(doopen){
	DashboardOpen = !DashboardOpen || doopen;
	if(DashboardOpen){
		dashboard.setAttribute("style", "left:250px");
		dashboardArticles.setAttribute("style", "z-index: 10000");
		document.getElementById("dashboardbutton").setAttribute("style", "left: -100px");
	} else {
		dashboard.setAttribute("style", "left:0px");
		dashboardArticles.setAttribute("style", "z-index:-1");
		document.getElementById("dashboardbutton").removeAttribute("style");
	}
	if(!DashboardOpen){
		$("#dashboard").one('transitionend webkitTransitionEnd oTransitionEnd otransitionend MSTransitionEnd', 
		function() {
			if(!DashboardOpen && !usingTouch){
				dashboard.removeAttribute("style");
			}
		});
		OpenElement();
		if(video.paused()){
			playPause();
		}
	} else if (!doopen){
		OpenElement("omprojektet");
	}
}
function OpenElement(element){
	if(OpenDashboardArticle){
		document.getElementById(OpenDashboardArticle).removeAttribute("style");
		document.getElementById(OpenDashboardArticle + "-knap").className = "";
		OpenDashboardArticle = undefined;
		document.getElementById("slumekspertvideo").pause();
		document.getElementById("slumdrengvideo").pause();
	}
	if(element){
		document.getElementById(element).setAttribute("style", "opacity: 1; z-index: 30");
		document.getElementById(element + "-knap").className = "open";
		OpenDashboardArticle = element;
		OpenDashboard(true);
		video.pause();
	}
}
