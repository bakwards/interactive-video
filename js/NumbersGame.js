﻿/**
 * @author Christian Kajsing

Load instruktions
<title>Your HTML Page</title>
<html lang="en">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <div id="Quiz"></div>

        <script src="js/NumbersGame.js"></script>
        <script>LoadQuestion("Hvormange nivoeer har Anders And?", ["1", "2", "3"], 2);</script>
    </body>
</html>
 */
var rightNumber = 0;
var answers = [];
var question = "";


function LoadQuestion(question, answers, rightNumber) //the question asked, answers in array, what spot in answers array is correct
{
    this.rightNumber = rightNumber;
    this.question = question;
    this.answers = answers.split(";");
    GetQuestion();
}

function GetQuestion() //get the question in question
{
    var cont = "<div class = \"headline\">Quiz</div><div class = \"content\"><div class = \"question\">" + question + " <div id = \"response\"> </div></div> " +
            "<ul class = \"answers\"><li onclick = \"Guess(0)\">" + answers[0] + " </li> " +
            "<li onclick = \"Guess(1)\">" + answers[1] + " </li> " +
            "<li onclick = \"Guess(2)\">" + answers[2] + " </li></ul></div>";

    document.getElementById("Quiz").innerHTML = cont;
}
function Guess(answer) // check if is right question.
{
    var obj;
    if (answer == rightNumber)
    {
        obj = "Sådan, du svarede rigtigt. Godt klaret!";
        //you won!
    }
    else {
        obj = "Desværre, det rigtige svar er '" + answers[rightNumber] + "'.";
        //you fail!   
    }
    document.getElementById("response").innerHTML = obj;
}






