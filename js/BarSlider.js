var bar, slider, info, quest, won;

var rightNumber = 0;
var set_perc = 0;
var answers = [];
var question = "";

var min, max, oneBarLenthPerc;

function LoadQuestion(question, answers, rightNumber) //the question asked, answers in array, what spot in answers array is correct
{
    this.rightNumber = rightNumber;
    this.question = question;
    this.answers = answers;
    init();
}

function FindMin()
{
    var m;
    for (var i = 0; i < answers.length; i++)
    {
        if (m == null || answers[i] < m)
        {
            m = answers[i];
        }
    }
    return m;
}
function FindMax() {
    var m;
    for (var i = 0; i < answers.length; i++) {
        if (m == null || answers[i] > m) {
            m = answers[i];
        }
    }
    return m;
}

function init()
{
    won = document.getElementById('won');
    bar = document.getElementById('bar');
    slider = document.getElementById('slider');
    info = document.getElementById('info');
    quest = document.getElementById('question');
    bar.addEventListener('mousedown', startSlide, false);
    bar.addEventListener('mouseup', stopSlide, false);

    quest.innerHTML = question;

    min = FindMin();
    max = FindMax();
    

    oneBarLenthPerc = (max - min) / 100;
    console.log(oneBarLenthPerc+"");
}

function startSlide(event)
{
    Update();
    bar.addEventListener('mousemove', moveSlide, false);
}

function moveSlide(event)
{
    Update();
}

function stopSlide(event)
{
    Update();
    bar.removeEventListener('mousemove', moveSlide, false);
}

function Closest(perc)
{
    var nearest;

    for (var i = 0; i < answers.length; i++)
    {
        if (nearest == null || Math.abs(answers[i] - (perc * oneBarLenthPerc)) < Math.abs(nearest - (perc * oneBarLenthPerc)))
        {
            console.log(perc * oneBarLenthPerc);
            nearest = answers[i];
        }
    }
    return nearest != null ? nearest : "";
}

function Update()
{
    set_perc = ((((event.clientX - bar.offsetLeft) / bar.offsetWidth)).toFixed(2));
    slider.style.width = (set_perc * 100) + '%';

    
    info.innerHTML = Closest(set_perc*100) + "";
    if (Closest(set_perc * 100) == answers[rightNumber])
    {
        won.innerHTML = "You are correct!";
    }
    else
    {
        won.innerHTML = "";
    }

}
