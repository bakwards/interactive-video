<?php

/**
 * @author Christian Kajsing
 * @copyright 2014
 */

$video = $_POST['videonr'];
//#if($video == "") {$video = 999;}
$article = $_POST['article'];
$linke = $_POST['link'];

require 'dbcon.php';
try {$dbh = new PDO($driver, $user, $pass, $attr);}
catch (exception $e) 
{
    echo 'Exception : ' . $e->getMessage() . "\n"; 
    die();
}

$insert = "INSERT INTO `mchriste_slumchallenge` . `tracking` (`id` ,`video_id` ,`article_name` ,`link` ,`time`) VALUES (NULL , :video , :article , :link , CURRENT_TIMESTAMP)";

try 
{
    $stmt = $dbh->prepare($insert);
    $stmt->bindParam(':video', $video, PDO::PARAM_INT);
    $stmt->bindParam(':article', $article, PDO::PARAM_STR);
    $stmt->bindParam(':link', $linke, PDO::PARAM_STR);
    $stmt->execute();
    unset($stmt);
} catch (exception $e)
{
    echo 'Exception : ' . $e->getMessage() . "\n"; 
    die();
}

?>